import {Injectable} from "@angular/core";
import {getJSON, request } from "tns-core-modules/http"; //tecnologia de consultas en la web
const sqlite = require("nativescript-sqlite");

@Injectable() //indica que esta clase va a ser inyectada a distintos comoinentes que van a declarar su dependencia en esta clase através de recibir una variable de este tipo en su constructor

export class  NoticiasService {
    api: string ="https://4ceaf1f3d1f4.ngrok.io";
    
    constructor(){
        this.getDb((db)=>{
            console.dir(db);
            db.each("select * from logs",
                (err, fila) => console.log("fila: ", fila),
                (err, totales) => console.log("Filas totales: ", totales));
        },() => console.log("error on getDB"));
    }
    getDb(fnOk, fnError) {
        return new sqlite("mi_db_logs",(err,db) => { //instancia para una conexion con db de SQlite con el primer parametro(Nombre db)
            if (err) {
                console.error("Error al abrir db!", err);
            } else {
                console.log("Está la db abierta: ", db.isOpen() ? "Si":"No");
                db.execSQL("CREATE TABLE IF NOT EXISTS logs (id INTEGER PRIMARY KEY AUTOINCREMENT, texto TEXT)") //creamos la tabla con esos campos
                    .then((id)=> {//AUTOINCREMENT incrementa el id automaticamente
                        console.log("CREATE TABLE OK");
                        fnOk(db);
                    }, (error) => {
                        console.log("CREATE TABLE ERROR", error);
                        fnError(error);
                    
                    });
            }
            
        });
    }

    agregar(s: string){ //metodo para agregar en el array
        return request({
            url: this.api + "/favs",
            method: "POST",
            headers: {"Content-Type": "application/json" },
            content: JSON.stringify({
                nuevo: s
            })
        });
    }
    favs() {
        return getJSON(this.api + "/favs");
    }
    buscar(s: string){ //recibe un string de busqueda y llamamos al api con  /get y q(query/consulta) ..mas el string(s) que ingresaron para consultar
      this.getDb((db) =>{ //hacemos un insert en la DB
          db.execSQL("insert into logs (texto) values (?)", [s],
          (err, id) => console.log("nuevo id: ", id));
      }, () => console.log("error on getDB"));

        return getJSON(this.api + "/get?q=" + s); // esta consulta la hacemos con el get JSON que importamos arriba con el core module.
    }
}