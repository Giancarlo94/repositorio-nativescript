import { Component, ElementRef, OnInit, ViewChild } from "@angular/core";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import * as app from "tns-core-modules/application";
import {registerElement} from "nativescript-angular/element-registry"; //maps

registerElement("MapView", () => require("nativescript-google-maps-sdk").MapView); //maps

@Component({
    selector: "Browse",
    templateUrl: "./browse.component.html"
})
export class BrowseComponent implements OnInit {
    @ViewChild("MapView" ,{static: false}) mapView: ElementRef; //maps sintaxis nueva, no seguí video

    constructor() {
        // Use the component constructor to inject providers.
    }

    ngOnInit(): void {
        // Init your component properties here.
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.showDrawer();
    }

    onMapready(event): void {
        console.log("Map Ready");
    }
}
