import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { EffectsModule } from "@ngrx/effects";
import { ActionReducerMap,StoreModule as NgRxStoreModule } from "@ngrx/store";
import { NativeScriptModule } from "nativescript-angular/nativescript.module";
import { NativeScriptUISideDrawerModule } from "nativescript-ui-sidedrawer/angular";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { NoticiasState, 
    initializeNoticiasState, 
    NoticiasEffects,        
    reducersNoticias } from "./browse/domain/noticias-state.model";
import { NoticiasService } from "./browse/domain/noticias.services";



//redux init
export interface AppState {
    noticias: NoticiasState;
}

const reducers: ActionReducerMap<AppState> = {
    noticias: reducersNoticias
};

const reducersInitialState = {
    noticias: initializeNoticiasState()
};
//redux fin


@NgModule({ //es un modulo raiz porque tiene un entrada Bootstrap
    bootstrap: [//inicializacion del modulo
        AppComponent//invoca appcomponent
    ],
    imports: [ 
        AppRoutingModule,//modulo de ruteo, modulos hijos del app modulo(app-routing.module.ts)
        NativeScriptModule,
        NativeScriptUISideDrawerModule,
        NgRxStoreModule.forRoot(reducers, { initialState: reducersInitialState}), //debe estar importado arriba
        EffectsModule.forRoot([NoticiasEffects])
    ],
    providers: [NoticiasService],
    declarations: [// componentes, directivas y pipes que este modulo declara para ser usadas por otro modulo
        AppComponent
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class AppModule { }
