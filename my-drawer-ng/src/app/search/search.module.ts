import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";

import { NativeScriptFormsModule } from "nativescript-angular/forms" //nos da la funcionalidad para que funcione el shuaBuildin(?)
import { SearchRoutingModule } from "./search-routing.module";

import { SearchComponent } from "./search.component";
import { NoticiasService } from "../browse/domain/noticias.services";
import { SearchFormComponent } from "./search-form.component";
import { NativeScriptModule } from "nativescript-angular/nativescript.module";
import { from } from "rxjs";

@NgModule({
    imports: [
        NativeScriptCommonModule,
        SearchRoutingModule,
        NativeScriptFormsModule
    ],
    declarations: [
        SearchComponent,
        SearchFormComponent, //Agrego el componente de formulario y verifico si se realizó el import arriba
         // libreria necesaria para funcionalidad de formularios
    
    ],
    //providers: [NoticiasService], //uno o mas componentes de este modulo van a poder utilizar objetos de este tipo, sin necesidad de implementar el providers en cada componente.
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class SearchModule { }
