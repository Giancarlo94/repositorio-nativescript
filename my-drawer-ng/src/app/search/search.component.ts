import { Component, OnInit, ViewChild, ElementRef } from "@angular/core";
import * as Toast from "nativescript-toasts";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import * as app from "tns-core-modules/application";
import {Color, View } from "tns-core-modules/ui/core/view/view";
import { AppState } from "../app.module";
import { NoticiasService } from "../browse/domain/noticias.services";
import {Store} from "@ngrx/store";
import { Noticia, NuevaNoticiaAction } from "../browse/domain/noticias-state.model";
import * as SocialShare from "nativescript-social-share"; 
@Component({
    selector: "Search",
    moduleId: module.id,
    templateUrl: "./search.component.html"
    //providers: [NoticiasService] //Si este componente(search.component.ts) recibe por parámetros en su contructor..
    //..esos valores deben proveerse por el inyector de dependencias de angular(en este caso lo definimos a nivel de comoonente, sin embargo es posible definirlo a nivel de módulo (search.module.ts)..es más eficaz)
    //Agregamos la clausula para poder usarla en cualquier modulo que utilice este componente(search.component.ts)
    //y no tener que poner esta línea de codigo en todos.
})
export class SearchComponent implements OnInit {

    resultados: Array<string>; //creo array que almacenará las entradas a buscar
    
    @ViewChild("layout",{  
        static: false  
    }) layout: ElementRef; //configuraremos dentro del buscarAhora el Layout* esta sintaxis la encontré en otro video
   
   
    constructor(private noticias: NoticiasService,
        private store: Store<AppState> //redux
        ) {//construcor que recibe variable de "noticias.services.ts", si no se importa esta clase automaticamente, debe importarse arriba*
    //La variable noticias se podrá usar en todo este componente, y en el html
    // Use the component constructor to inject providers.
   
    }

    ngOnInit(): void {
        // Inicializa los componentes antes de renderear y luego de haberlos  contruidos

        this.store.select((state) => state.noticias.sugerida)
            .subscribe((data) => {
                const f = data;
                if (f !=null) {
                    Toast.show({text: "Sugerimos leer: " + f.titulo, duration: Toast.DURATION.SHORT});
                }
            })
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.showDrawer();
    }
    onItemTap(args):void{
        this.store.dispatch(new NuevaNoticiaAction(new Noticia(args.view.bindingContext)));
       
    }
    onLongPress(s): void{
        console.log(s);
        SocialShare.shareText(s,"Asunto: compoartido desde el curso!");

    }
    
    buscarAhora(s: string){ //Funcion que recibe el string ingresado
        console.dir("buscarAhora"+ s); //ENtra al buscarAhora
        this.noticias.buscar(s).then((r: any) => {//luego, llamamos a buscar(el del search module) nos retorna una promesa, y sobre la promesa llamamos al mpetodo THEN
            console.log("resultados buscarAhora:" + JSON.stringify(r));// tenemos dos callbacks aquí: 1cuando llega el resultado el retorno favorable, lo mostramos por consola y lo asignamos a resultados(Variable del front end)
            this.resultados =r; 
        }, (e) => {//2. En caso de error, también lo logueamos y usamos en Toast
            console.log("error buscarAhora" + e);
            Toast.show({text: "Error en la búsqueda", duration: Toast.DURATION.SHORT});
        });
    }
}

    



