import { Component, EventEmitter, Output } from "@angular/core";

@Component({
    selector: "SearchForm",
    moduleId: module.id,
    template: `<FlexboxLayout flexDirection="row">
    <TextField #texto="ngModel" [(ngModel)]="textFieldValue" hint="Ingresar texto ..."
    required></TextField>
    <Label *ngIf="texto.hasError('required')" text="*"></Label>
   </FlexboxLayout>
   <Button text="Buscar!" (tap)="onButtonTap()" *ngIf="texto.valid"></Button>
    ` 
})
    //Comillas invertidas para poner esta cadena de texto html, con esto nos ahorramos el componente html
    //lo que ingresen en el textfiel, se almacenará en la variable textFieldValue.
    //Vuando hagan clic en buscar, llamanos a OnbuttonTap
export class SearchFormComponent {
    textFieldValue: string = ""; //variable TS que recibe lo del txtfield de arriba
    @Output() search: EventEmitter<string> = new EventEmitter(); //emitimos un evento search, usado en el componente html

    onButtonTap(): void {
        console.log(this.textFieldValue);
        if (this.textFieldValue.length > 2) {
            this.search.emit(this.textFieldValue);

        }
    }
}