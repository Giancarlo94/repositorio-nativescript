import { Component, OnInit } from "@angular/core";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import * as app from "tns-core-modules/application";
import {GestureEventData} from "tns-core-modules/ui/gestures/gestures";
import {GridLayout} from "tns-core-modules/ui/layouts/grid-layout"
import { from } from "rxjs";

@Component({
    selector: "Featured",
    moduleId: module.id,
    templateUrl: "./featured.component.html"
})
export class FeaturedComponent implements OnInit {

    constructor() {
        // Use the component constructor to inject providers.
    }

    ngOnInit(): void {
        // Init your component properties here.
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.showDrawer();
    }

    onLongPress(args: GestureEventData) {
        console.log("Objetc that triggered the event: " + args.object);
        console.log("View that triggered the event: " + args.view);
        console.log("Event name: " + args.eventName);

        const grid = <GridLayout>args.object; //casteo de datos* indagar
        grid.rotate = 0; // arranca desde 0 grados
        grid.animate({
            rotate: 360,// y da una vuelta entera que dura 2 segundos
            duration: 2000
        });
        

    }
}
