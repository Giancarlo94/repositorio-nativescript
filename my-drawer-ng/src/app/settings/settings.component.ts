import { Component, OnInit } from "@angular/core";
import * as Toast from "nativescript-toasts";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import * as app from "tns-core-modules/application";

import * as dialogs from "tns-core-modules/ui/dialogs" //import para dialogos

@Component({
    selector: "Settings",
    moduleId:module.id,
    templateUrl: "./settings.component.html"
})
export class SettingsComponent implements OnInit {

    constructor() {
        // Use the component constructor to inject providers.
    }

    doLater(fn) { setTimeout(fn,1000);} //funcion auxiliar que recibe otra funcion y se ejecuta despues de llamar el metodo de abajo(doLater)

    ngOnInit(): void {
        // Init your component properties here.... FUNCION PARA VENTANA EMERGENTE CON OPCIONES
          this.doLater(()=>//Funcion que genera la ventana emergente con las dos opciones para elegir, funcionó*
            dialogs.action("Mensaje","Cancelar!",["Opcion1","Opcion2"])
                .then((result) =>{
                                    console.log("resultado: " + result);
                                    if (result === "Opcion1"){
                                        this.doLater(()=>
                                            dialogs.alert({
                                                title: "Titulo 1 ",
                                                message: "mje 1",
                                                okButtonText: "bnt 1"    
                                            }).then(()=> console.log("Cerrado 1!")));
                                            
                                    } else if (result === "Opcion2"){
                                        this.doLater(()=>
                                        dialogs.alert({
                                            title: "Titulo 2 ",
                                            message: "mje 2",
                                            okButtonText: "bnt 2"    
                                        }).then(()=> console.log("Cerrado 2!")));
                                    }
                }));
   
     /*     const toastOptions: Toast.ToastOptions = {text: "Hello Word", duration: Toast.DURATION.SHORT};
        this.doLater(() => Toast.show(toastOptions)); */
 
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.showDrawer();
    }
}
