var express= require("express"), cors = require('cors'); //utilizamos la librería express y CORS: libreria que permite que nuestra app reciba solicitudes desde cualquier parte(consulta o agregado de info..esta declarado en el package.JSON)
var app = express(); //inicializo framework xpress
app.use(express.json()); //Usaremos el modulo de xpress para el manejo de JSON
app.use(cors());//usamos cors
app.listen(3000,() => console.log("Server running on port 3000"));//nuestra app estara en el puerto 3000

var noticias =[
    "Literatura Paris", "Futbol Barcelona", "Futbol Barranquilla",
    "Política Montevideo", "Economía Santiago de Chile", "Cocina México DF",
    "Finanzas Nueva York"
];
//GET: punto de entrada de consulta, que leera la info del servidor web en /get
//Descargaremos un JSON, el cual tendrá unArray de noticias y filtraremos de acuerdo un texto de filtrado que nos pasaran por query string en la URL
//el filter retorna un array filtrado.
app.get("/get", (req, res, next)  =>
    res.json(noticias.filter((c) => c.toLowerCase().indexOf(req.query.q.toString().toLowerCase()) > -1 )));

var misFavoritos = [];
app.get("/favs", (req,res,next) => res.json(misFavoritos));
app.post("/favs",(req, res, next) => {
    console.log(req.body);
    misFavoritos.push(req.body.nuevo);
    res.json(misFavoritos);
});